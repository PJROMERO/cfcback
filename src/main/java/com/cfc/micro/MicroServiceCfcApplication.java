package com.cfc.micro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MicroServiceCfcApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroServiceCfcApplication.class, args);
	}

}
