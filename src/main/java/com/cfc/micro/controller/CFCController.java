package com.cfc.micro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cfc.micro.service.serviceCFC;

@RestController
@RequestMapping("/api/cfc")
public class CFCController {
	
	@Autowired
	serviceCFC seCfc;
	
	@RequestMapping("/")
	@ResponseBody
	String test() {
		
		return seCfc.getMensaje();
	}

}
